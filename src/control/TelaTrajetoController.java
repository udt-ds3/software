/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import model.Main;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author Aluno
 */
public class TelaTrajetoController implements Initializable {
    private String fxml="TelaTrajeto.fxml";
    private static String pgProxima = null;
    public static void setPgProxima(String pgProxima) {
        TelaTrajetoController.pgProxima = pgProxima;
    }
   private static String pgAnterior = "TelaAdm.fxml";

    public static void setPgAnterior(String pgAnterior) {
        TelaTrajetoController.pgAnterior = pgAnterior;
        TelaTrajetoController.pgProxima = null;
    }
    @FXML
    private Button volta;
    @FXML
    private Button frente;
    private Button frente1;
    @FXML
    private MenuItem usuarios;
    @FXML
    private TextField nCompleto;
    @FXML
    private TextField buscaEnd;
    @FXML
    private ListView<?> listaEnd;
    @FXML
    private Label endereco;
    @FXML
    private Label avisoEnd;
    @FXML
    private TextField nCompleto1;
    @FXML
    private TextField buscaEnd1;
    @FXML
    private ListView<?> listaEnd1;
    @FXML
    private Label endereco1;
    @FXML
    private Label avisoEnd1;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        if(pgProxima==null)
        {
            frente.setDisable(true);
        }
        else
        {
            frente.setDisable(false);
        }
    }   

    public String getFxml() {
        return fxml;
    }

    public void setFxml(String fxml) {
        this.fxml = fxml;
    }

    public Button getVolta() {
        return volta;
    }

    public void setVolta(Button volta) {
        this.volta = volta;
    }

    public Button getFrente() {
        return frente;
    }

    public void setFrente(Button frente) {
        this.frente = frente;
    }

    public Button getFrente1() {
        return frente1;
    }

    public void setFrente1(Button frente1) {
        this.frente1 = frente1;
    }

    public MenuItem getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(MenuItem usuarios) {
        this.usuarios = usuarios;
    }
    
    
    @FXML
    public void voltar(){
        Main.trocaTela(pgAnterior);
    }

    @FXML
    public void trocaEndereco()  {
         TelaEnderecoController.setPgAnterior(fxml);
         setPgProxima("TelaEndereco.fxml");
         Main.trocaTela(pgProxima);
    }
    
    public void trocaResponsavel()  {
        TelaResponsavelController.setPgAnterior("TelaCliente.fxml");
        Main.trocaTela("TelaResponsavel.fxml");
    }
   

    @FXML
    private void frente(ActionEvent event) {
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void menu(ActionEvent event) {
        Main.trocaTela(TelaInicialController.getMenu());
    }

    @FXML
    private void trocaCliente(ActionEvent event) {
        TelaClienteController.setPgAnterior(fxml);
        setPgProxima("TelaCliente.fxml");
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void trocaViagem(ActionEvent event) {
        TelaViagensController.setPgAnterior(fxml);
        setPgProxima("TelaViagens.fxml");
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void trocaMotorista(ActionEvent event) {
        TelaMotoristaController.setPgAnterior(fxml);
        setPgProxima("TelaMotorista.fxml");
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void trocaVeiculo(ActionEvent event) {
        TelaVeiculoController.setPgAnterior(fxml);
        setPgProxima("TelaVeiculo.fxml");
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void trocaTrajeto(ActionEvent event) {
        //nada
    }

    @FXML
    private void trocaUsuario(ActionEvent event) {
        Main.trocaTela("TelaInicial.fxml");
    }

    
    @FXML
    private void trocaTVeiculo(ActionEvent event) {
        TelaTipoVeiculosController.setPgAnterior(fxml);
        setPgProxima("TelaTipoVeiculos.fxml");
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void trocaTCombustivel(ActionEvent event) {
        TelaTipoCombustivelController.setPgAnterior(fxml);
        setPgProxima("TelaTipoCombustiveis.fxml");
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void trocaTPagamento(ActionEvent event) {
        TelaPagamentoController.setPgAnterior(fxml);
        setPgProxima("TelaPagamento.fxml");
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void trocaTFrequencia(ActionEvent event) {
        TelaFrequenciasController.setPgAnterior(fxml);
        setPgProxima("TelaFrequencias.fxml");
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void sair(ActionEvent event) {
        Main.trocaTela("TelaInicial.fxml");
    }

    @FXML
    private void limpaCampo2(MouseEvent event) {
    }

    @FXML
    private void addItems2(KeyEvent event) {
    }

    @FXML
    private void sel2(MouseEvent event) {
    }
}
