/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import model.Main;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author Aluno
 */
public class TelaViagensController implements Initializable {
    /**
     * Initializes the controller class.
     */   
    private static String pgProxima = null;
    public static void setPgProxima(String pgProxima) {
        TelaViagensController.pgProxima = pgProxima;
    }
    private String fxml="TelaViagens.fxml";
    private static String pgAnterior = "TelaAdm.fxml";

    public static void setPgAnterior(String pgAnterior) {
        TelaViagensController.pgAnterior = pgAnterior;
        TelaViagensController.pgProxima = null;
    }
    @FXML
    private Button volta;
    @FXML
    private Button frente;
    @FXML
    private MenuItem usuarios;
    @FXML
    private TextField buscaMot;
    @FXML
    private ListView<?> listaMot;
    @FXML
    private Label motorista;
    @FXML
    private Label avisoMot;
    @FXML
    private TextField buscaVei;
    @FXML
    private ListView<?> listaVei;
    @FXML
    private Label veiculo;
    @FXML
    private Label avisoVei;
    @FXML
    private TextField buscaFreq;
    @FXML
    private ListView<?> listaFreq;
    @FXML
    private Label frequencia;
    @FXML
    private Label avisoFreq;
    @FXML
    private ListView<?> lista;
    @FXML
    private Label avisos;
    @FXML
    private TextField busca;
    @FXML
    private TableView<?> tabelaDados;
    @FXML
    private TableColumn<?, ?> tipoT;
    @FXML
    private TableColumn<?, ?> numT;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        if(pgProxima==null)
        {
            frente.setDisable(true);
        }
        else
        {
            frente.setDisable(false);
        }
    }    
    
    @FXML
    public void voltar(){
        Main.trocaTela(pgAnterior);
    }

    @FXML
    public void trocaEndereco()  {
         TelaEnderecoController.setPgAnterior(fxml);
         setPgProxima("TelaEndereco.fxml");
         Main.trocaTela(pgProxima);
    }
    
    public void trocaResponsavel()  {
        TelaResponsavelController.setPgAnterior("TelaCliente.fxml");
        Main.trocaTela("TelaResponsavel.fxml");
    }
   

    @FXML
    private void frente(ActionEvent event) {
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void menu(ActionEvent event) {
        Main.trocaTela(TelaInicialController.getMenu());
    }

    @FXML
    private void trocaCliente(ActionEvent event) {
        TelaClienteController.setPgAnterior(fxml);
        setPgProxima("TelaCliente.fxml");
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void trocaViagem(ActionEvent event) {
        //nada
    }

    @FXML
    private void trocaMotorista(ActionEvent event) {
        TelaMotoristaController.setPgAnterior(fxml);
        setPgProxima("TelaMotorista.fxml");
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void trocaVeiculo(ActionEvent event) {
        TelaVeiculoController.setPgAnterior(fxml);
        setPgProxima("TelaVeiculo.fxml");
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void trocaTrajeto(ActionEvent event) {
        TelaTrajetoController.setPgAnterior(fxml);
        setPgProxima("TelaTrajeto.fxml");
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void trocaUsuario(ActionEvent event) {
        Main.trocaTela("TelaInicial.fxml");
    }

    @FXML
    private void trocaTVeiculo(ActionEvent event) {
        TelaTipoVeiculosController.setPgAnterior(fxml);
        setPgProxima("TelaTipoVeiculos.fxml");
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void trocaTCombustivel(ActionEvent event) {
        TelaTipoCombustivelController.setPgAnterior(fxml);
        setPgProxima("TelaTipoCombustiveis.fxml");
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void trocaTPagamento(ActionEvent event) {
        TelaPagamentoController.setPgAnterior(fxml);
        setPgProxima("TelaPagamento.fxml");
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void trocaTFrequencia(ActionEvent event) {
        TelaFrequenciasController.setPgAnterior(fxml);
        setPgProxima("TelaFrequencias.fxml");
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void sair(ActionEvent event) {
        Main.trocaTela("TelaInicial.fxml");
    }

    @FXML
    private void limpaCampo2(MouseEvent event) {
    }

    @FXML
    private void addItems2(KeyEvent event) {
    }

    @FXML
    private void sel2(MouseEvent event) {
    }

    @FXML
    private void cadastrar(ActionEvent event) {
    }

    @FXML
    private void limpaCampo(ActionEvent event) {
    }

    @FXML
    private void selecionar(MouseEvent event) {
    }

    @FXML
    private void editar(ActionEvent event) {
    }

    @FXML
    private void excluir(ActionEvent event) {
    }

    @FXML
    private void limpaCampo(MouseEvent event) {
    }

    @FXML
    private void addItems(KeyEvent event) {
    }

    @FXML
    private void byTV(ActionEvent event) {
    }

    @FXML
    private void byTC(ActionEvent event) {
    }
}
