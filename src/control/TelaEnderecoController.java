package control;

import model.Main;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import model.Endereco;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

/**
 * FXML Controller class
 *
 * @author Aluno
 */
public class TelaEnderecoController implements Initializable {

    private Endereco novo = new Endereco();
    private String fxml = "TelaEndereco.fxml";

    private static String pgProxima = null;

    private static String pgAnterior = null;
    @FXML
    private TextField pais;
    @FXML
    private TextField cep;
    @FXML
    private TextField rua;
    @FXML
    private TextField cidade;
    @FXML
    private TextField numero;
    @FXML
    private TextField bairro;
    @FXML
    private Button volta;
    @FXML
    private Button frente;
    @FXML
    private MenuItem usuarios;
    @FXML
    private TextField complemento;
    @FXML
    private TextField tempoDeslocamento;
    @FXML
    private TabPane tabs;
    @FXML
    private Tab tabNovo;
    @FXML
    private TextField estado;
    @FXML
    private Label avisos;
    @FXML
    private Tab tabExistente;
    @FXML
    private Tab tabCep;
    @FXML
    private TableView<Endereco> tabela;
    @FXML
    private TableColumn<?, ?> cepT;
    @FXML
    private TableColumn<?, ?> ruaT;
    @FXML
    private TableColumn<?, ?> numT;
    @FXML
    private TableColumn<?, ?> bairroT;
    @FXML
    private TableColumn<?, ?> cidadeT;
    @FXML
    private TableColumn<?, ?> estadoT;
    @FXML
    private TableColumn<?, ?> paisT;
    @FXML
    private TableColumn<?, ?> tempoT;
    @FXML
    private Button editarButton;
    @FXML
    private Button excluirButton;
    @FXML
    private TextField id;
    @FXML
    private TextField busca;
    @FXML
    private TableView<Endereco> tabela2;
    @FXML
    private TableColumn<?, ?> regiaoT;
    @FXML
    private TableColumn<?, ?> nRegT;
    @FXML
    private TableColumn<?, ?> deslocT;
    @FXML
    private Label avisos2;
    @FXML
    private TextField ciCep;
    @FXML
    private TextField ufCep;
    @FXML
    private TextField loCep;
    @FXML
    private ListView<Endereco> lista;
    @FXML
    private TextArea enderecoCompleto;
    @FXML
    private Label avisosCepBusca;

    /**
     * Limpa os campos de avisos.
     */
    @FXML
    private void limpaCampo() {
        avisos.setText("");
        avisosCepBusca.setText("");
        avisos2.setText("");
    }

    /**
     * Limpa os campos de preenchimento.
     */
    @FXML
    private void limpaTexto() {
        pais.setText("Brasil");
        cep.setText("");
        estado.setText("");
        cidade.setText("");
        bairro.setText("");
        rua.setText("");
        numero.setText("");
        complemento.setText("");
        tempoDeslocamento.setText("");
        id.setText(null);
        enderecoCompleto.setText("");
    }

    /**
     * Seta o fxml da página que irá com o botão avançar.
     */
    public static void setPgProxima(String pgProxima) {
        TelaEnderecoController.pgProxima = pgProxima;
    }

    /**
     * Seta o fxml da página que irá com o botão voltar.
     */
    public static void setPgAnterior(String pgAnterior) {
        TelaEnderecoController.pgAnterior = pgAnterior;
        TelaEnderecoController.pgProxima = null;
    }

    /**
     * Pega os endereços cadastrados e mostra na tela.
     */
    @FXML
    private void addItems() {
        try {
            tabela.getItems().clear();

            for (Endereco e : Endereco.getAll()) {
                if (busca.getText() == null || e.containsText((busca.getText()))) {
                    tabela.getItems().add(e);
                }
            }
        } catch (Exception e) {
            System.out.println("");
        }
    }

    /**
     * Pega os dados dos registros e mostra na tela.
     */
    private void addItems2(String campo) {
        try {
            tabela2.getItems().clear();

            for (Endereco e : Endereco.getGroupBy(campo)) {
                tabela2.getItems().add(e);
            }
        } catch (Exception e) {
            System.out.println("");
        }
    }

    /**
     * Cria colunas da tabela da página de cadastrados.
     */
    private void makeColumns() {
        cepT.setCellValueFactory(new PropertyValueFactory<>("cep"));
        ruaT.setCellValueFactory(new PropertyValueFactory<>("rua"));
        numT.setCellValueFactory(new PropertyValueFactory<>("numero"));
        bairroT.setCellValueFactory(new PropertyValueFactory<>("bairro"));
        cidadeT.setCellValueFactory(new PropertyValueFactory<>("cidade"));
        estadoT.setCellValueFactory(new PropertyValueFactory<>("estado"));
        paisT.setCellValueFactory(new PropertyValueFactory<>("pais"));
        tempoT.setCellValueFactory(new PropertyValueFactory<>("tempo"));
    }

    /**
     * Cria colunas da tabela da página de registros.
     */
    private void makeColumns2() {
        regiaoT.setCellValueFactory(new PropertyValueFactory<>("complemento"));
        nRegT.setCellValueFactory(new PropertyValueFactory<>("numero"));
        deslocT.setCellValueFactory(new PropertyValueFactory<>("tempo"));
    }

    /**
     * Pega o endereço que o usuário selecionou.
     */
    @FXML
    private void selecionarEndereco() {
        novo = tabela.getSelectionModel().getSelectedItem();
        enderecoCompleto.setText(novo.enderecoCompleto());
        editarButton.setDisable(false);
        excluirButton.setDisable(false);
    }
    
    /**
     * Testa se os campos obrigatórios estão preenchidos corretamente.
     */
    private int testaCampo() {
        if ("".equals(pais.getText())) {
            return 1;
        } else if ("".equals(cep.getText())) {
            return 1;
        } else if ("".equals(rua.getText())) {
            return 1;
        } else if ("".equals(cidade.getText())) {
            return 1;
        } else if ("".equals(numero.getText())) {
            return 1;
        } else if ("".equals(estado.getText())) {
            return 1;
        } else if ("".equals(bairro.getText())) {
            return 1;
        } else if ("".equals(tempoDeslocamento.getText())) {
            return 1;
        }
        else if("Brasil".equals(pais.getText()) && cep.getText().charAt(5)!='-')
        {
            return 1;
        }
        return 0;
    }
    
    /**
     * Salva novas informações.
     */
    @FXML
    private void cadastrar() {
        if (testaCampo() == 0) {

            novo.setPais(pais.getText());
            novo.setEstado(estado.getText());
            novo.setCidade(cidade.getText());
            novo.setBairro(bairro.getText());
            novo.setRua(rua.getText());
            novo.setNumero(Integer.parseInt(numero.getText()));
            novo.setComplemento(complemento.getText());
            novo.setCep(cep.getText());
            novo.setTempo(Integer.parseInt(tempoDeslocamento.getText()));

            if (id.getText() != null) {
                if(novo.update()){
                    avisos.setText("Endereço atualizado");
                    limpaTexto();
                }
                else{
                    avisos.setText("Não foi possível atualizar o endereço.");
                }
            } else {
                if (novo.insert()) {
                    avisos.setText("Endereço cadastrado com id " + novo.getId());
                    limpaTexto();
                } else {
                    avisos.setText("Não foi possível cadastrar o endereço.");
                }
            }

        } else {
            avisos.setText("Revise o preenchimento dos campos.");
        }

    }
    
    /**
     * Método inicial do controlador.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        makeColumns();
        makeColumns2();
        id.setText(null);
        if (pgProxima == null) {
            frente.setDisable(true);
        } else {
            frente.setDisable(false);
        }
    }
    
    /**
     * Vai para a tela anterior.
     */
    @FXML
    private void voltar() {
        Main.trocaTela(pgAnterior);
    }
    
    /**
     * Vai para a tela anterior.
     */
    @FXML
    private void trocaEndereco() {
        //nada
    }
    
    /**
     * Vai para a tela posterior.
     */
    @FXML
    private void frente(ActionEvent event) {
        Main.trocaTela(pgProxima);
    }
    
    /**
     * Vai para a tela de menu.
     */
    @FXML
    private void menu(ActionEvent event) {
        Main.trocaTela(TelaInicialController.getMenu());
    }
    
    /**
     * Vai para a tela de cliente.
     */
    @FXML
    private void trocaCliente(ActionEvent event) {
        TelaClienteController.setPgAnterior(fxml);
        setPgProxima("TelaCliente.fxml");
        Main.trocaTela(pgProxima);
    }
    
    /**
     * Vai para a tela de viagem.
     */
    @FXML
    private void trocaViagem(ActionEvent event) {
        TelaViagensController.setPgAnterior(fxml);
        setPgProxima("TelaViagens.fxml");
        Main.trocaTela(pgProxima);
    }
    
    /**
     * Vai para a tela do motorista.
     */
    @FXML
    private void trocaMotorista(ActionEvent event) {
        TelaMotoristaController.setPgAnterior(fxml);
        setPgProxima("TelaMotorista.fxml");
        Main.trocaTela(pgProxima);
    }
    
    /**
     * Vai para a tela de veiculo.
     */
    @FXML
    private void trocaVeiculo(ActionEvent event) {
        TelaVeiculoController.setPgAnterior(fxml);
        setPgProxima("TelaVeiculo.fxml");
        Main.trocaTela(pgProxima);
    }
    
    /**
     * Vai para a tela de trajeto.
     */
    @FXML
    private void trocaTrajeto(ActionEvent event) {
        TelaTrajetoController.setPgAnterior(fxml);
        setPgProxima("TelaTrajeto.fxml");
        Main.trocaTela(pgProxima);
    }
    
    /**
     * Vai para a tela de usuario
     */
    @FXML
    private void trocaUsuario(ActionEvent event) {
        Main.trocaTela("TelaInicial.fxml");
    }
    
    /**
     * Vai para a tela veiculo.
     */
    @FXML
    private void trocaTVeiculo(ActionEvent event) {
        TelaTipoVeiculosController.setPgAnterior(fxml);
        setPgProxima("TelaTipoVeiculos.fxml");
        Main.trocaTela(pgProxima);
    }
    
    /**
     * Vai para a tela de Tcombustivel.
     */
    @FXML
    private void trocaTCombustivel(ActionEvent event) {
        TelaTipoCombustivelController.setPgAnterior(fxml);
        setPgProxima("TelaTipoCombustiveis.fxml");
        Main.trocaTela(pgProxima);
    }
    
    /**
     * Vai para a tela de Tpagamento.
     */
    @FXML
    private void trocaTPagamento(ActionEvent event) {
        TelaPagamentoController.setPgAnterior(fxml);
        setPgProxima("TelaPagamento.fxml");
        Main.trocaTela(pgProxima);
    }
    
    /**
     * Vai para a tela Tfrequencia.
     */
    @FXML
    private void trocaTFrequencia(ActionEvent event) {
        TelaFrequenciasController.setPgAnterior(fxml);
        setPgProxima("TelaFrequencias.fxml");
        Main.trocaTela(pgProxima);
    }
    
    /**
     * Vai para a tela inicial
     */
    @FXML
    private void sair(ActionEvent event) {
        Main.trocaTela("TelaInicial.fxml");
    }
    
    /**
     * Adiciona outros dados a partir do CEP.
     */
    @FXML
    private void cepAdc() {
        int cont = 0;
        for (int f = 0; f < cep.getText().length(); f++) {
            try {
                Integer.parseInt("" + cep.getText().charAt(f));
                cont++;
            } catch (Exception e) {
            }
        }

        if (cont == 8 && pais.getText().equals("Brasil")) {
            String cepS = cep.getText();
            try {
                RestTemplate restTemplate = new RestTemplate();
                ResponseEntity<String> response = restTemplate.getForEntity("https://viacep.com.br/ws/" + cepS + "/json/", String.class);
                rua.setText(response.getBody().split("\"")[7]);
                cep.setText(response.getBody().split("\"")[3]);
                estado.setText(response.getBody().split("\"")[23]);
                bairro.setText(response.getBody().split("\"")[15]);
                cidade.setText(response.getBody().split("\"")[19]);
                complemento.setText(response.getBody().split("\"")[11]);
                avisos.setText("");
            } catch (Exception e) {
                avisos.setText("CEP não encontrado");
                //limpaTexto();
                //cep.setText(cepS);
            }

        }

    }
    
    /**
     * Vai para a tab de buscar cep.
     */
    @FXML
    private void buscaCep(MouseEvent event) {
        tabs.getSelectionModel().select(tabCep);
    }
    
    /**
     * Vai para a tab de editar endereço e seta valores.
     */
    @FXML
    private void editar(ActionEvent event) {
        id.setText(novo.getId() + "");
        pais.setText(novo.getPais() + "");
        cep.setText(novo.getCep() + "");
        cidade.setText(novo.getCidade() + "");
        estado.setText(novo.getEstado() + "");
        bairro.setText(novo.getBairro() + "");
        rua.setText(novo.getRua() + "");
        numero.setText(novo.getNumero() + "");
        complemento.setText(novo.getComplemento() + "");
        tempoDeslocamento.setText("" + novo.getTempo());
        tabs.getSelectionModel().select(tabNovo);
    }
    
    /**
     * Exclui endereço selecionado.
     */
    @FXML
    private void excluir(ActionEvent event) {
        if (!novo.delete()) {
            avisos2.setText("Endereço em uso. Exclusão não efetuada.");
        }
        else
        {
            avisos2.setText("Endereço excluído.");
            addItems();
        }
    }
    
    /**
     * Agrupa dados registrados por ruas.
     */
    @FXML
    private void byRua(ActionEvent event) {
        addItems2("rua, bairro, cidade, estado");
    }
    
    /**
     * Agrupa dados registrados por bairros.
     */
    @FXML
    private void byBairro(ActionEvent event) {
        addItems2("bairro, cidade, estado");
    }
    
    /**
     * Agrupa dados registrados por cidades.
     */
    @FXML
    private void byCidade(ActionEvent event) {
        addItems2("cidade, estado");
    }
    
    /**
     * Agrupa dados registrados por estados.
     */
    @FXML
    private void byEstado(ActionEvent event) {
        addItems2("estado");
    }
    
    /**
     * Agrupa dados registrados por paises.
     */
    @FXML
    private void byPais(ActionEvent event) {
        addItems2("pais");
    }
    
    /**
     * Busca o CEP pela UF, cidade e logradouro.
     */
    @FXML
    private void buscarCep() {
        lista.getItems().clear();
        try {
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<String> response = restTemplate.getForEntity("https://viacep.com.br/ws/" + ufCep.getText() + "/" + ciCep.getText() + "/" + loCep.getText() + "/json/", String.class);
            Endereco e;
            for (String s : response.getBody().split("}")) {
                try {
                    e = new Endereco();
                    e.setPais("Brasil");
                    e.setEstado(s.split("\"")[23]);
                    e.setBairro(s.split("\"")[15]);
                    e.setCidade(s.split("\"")[19]);
                    e.setRua(s.split("\"")[7]);
                    e.setComplemento(s.split("\"")[11]);
                    e.setCep(s.split("\"")[3]);
                    lista.getItems().add(e);
                } catch (Exception exc) {
                    break;
                }
            }
        } catch (Exception exc2) {
            avisosCepBusca.setText("CEP não encontrado. Revise o valor dos campos.");
        }

    }
    
    /**
     * Pega o CEP que o usuário selecionou.
     */
    @FXML
    private void pegaCpf(MouseEvent event) {
        novo = lista.getSelectionModel().getSelectedItem();
        pais.setText("Brasil");
        cep.setText(novo.getCep() + "");
        cidade.setText(novo.getCidade() + "");
        estado.setText(novo.getEstado() + "");
        bairro.setText(novo.getBairro() + "");
        rua.setText(novo.getRua() + "");
        complemento.setText(novo.getComplemento() + "");
        tabs.getSelectionModel().select(tabNovo);
    }

}
