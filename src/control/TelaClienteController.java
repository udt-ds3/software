/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import model.Main;
import java.awt.Image;
import java.io.FileInputStream;
import java.io.IOException;
import static java.lang.System.exit;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import static java.util.Collections.list;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Side;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import model.Cliente;
import model.ClienteMaior;
import model.ClienteMenor;
import model.Endereco;
import model.Pessoa;
import model.TipoCombustivel;
import model.TipoPagamento;


/**
 * FXML Controller class
 *
 * @author Aluno
 */
public class TelaClienteController implements Initializable {

    private ClienteMaior novoMaior = new ClienteMaior();
     private ClienteMenor novoMenor = new ClienteMenor();
    private Cliente novo = new Cliente();
    private String fxml = "TelaCliente.fxml";
    private static String pgProxima = null;
    private static String pgAnterior = "TelaAdm.fxml";

    @FXML
    private TextField nCompleto;

    @FXML
    private TextField nascimento;
    @FXML
    private TextField telefone;
    private Label textNome;
    private Label textNascimento;
    private Label textTelefone;
    
    Date hoje = new Date();
    @FXML
    private Label responsavel;
    private Button responsavelAdd;
    @FXML
    private Button volta;
    @FXML
    private Button frente;
    @FXML
    private MenuItem usuarios;
    private int testa;
    public static void setPgAnterior(String pgAnterior) {
        TelaClienteController.pgAnterior = pgAnterior;
        TelaClienteController.pgProxima = null;
    }

    public static void setPgProxima(String pgProxima) {
        TelaClienteController.pgProxima = pgProxima;
    }
    private MenuButton boleto;
    @FXML
    private TabPane tabs;
    @FXML
    private Tab tabNovo;
    @FXML
    private Tab tabExistente;
    @FXML
    private TextField buscaTP;
    @FXML
    private Label tipoPagamento;
    @FXML
    private ListView<Endereco> listaEnd;
    private Endereco end = null;    
    @FXML
    private ListView<TipoPagamento> listaTP;
    private TipoPagamento tP = null;
    @FXML
    private TableView<Cliente> tabela;
    @FXML
    private Button editarButton;
    @FXML
    private Button excluirButton;
    @FXML
    private TextField busca;
    @FXML
    private TextArea clienteCompleto;
    @FXML
    private Label avisos2;
    @FXML
    private TableColumn<?, ?> nomeT;
    private TableColumn<?, ?> telefoneT;
    @FXML
    private TableColumn<?, ?> nascimentoT;
    @FXML
    private TableColumn<?, ?> pagamentoT;
    @FXML
    private Label avisos;
    @FXML
    private TableColumn<?, ?> numeroT;
    @FXML
    private TextField buscaEnd;
    @FXML
    private TextField buscaRes;
    @FXML
    private ListView<Cliente> listaRes;
    private Cliente lR = null;

    @FXML
    private Label endereco;
    @FXML
    private Button responsavelBt;
    @FXML
    private Label cpf;
    @FXML
    private TextField cpfTf;
    @FXML
    private Label aviso3;
    
    @FXML
    private Label responsavelLb;
    @FXML
    private Label avisoEnd;
    @FXML
    private Label avisoTp;
    @FXML
    private Label avisoRes;
    
    @FXML
    public void limpaCampo() {
        nCompleto.setText("");
        telefone.setText("");
        nascimento.setText("");
        endereco.setText("");
        tipoPagamento.setText("");
        responsavelLb.setText("");
        cpfTf.setText("");  
       // iid.setText(null);
        limp();
        limp2();
    }
    
    
    @FXML
    public void limpaCampo2() {
        endereco.setText("");
    }
     @FXML
    public void limpaCampo3() {
        tipoPagamento.setText("");
    }

     @FXML
    public void limpaCampo4() {
        responsavelLb.setText("");
    }
    

    public int testaCampo() {
       
         if ("".equals(nCompleto.getText())) {
            return 1;
        } else if ("".equals(telefone.getText())) {
            return 1;
        } else if ("".equals(nascimento.getText())) {
            return 1;
        }
        else if("".equals(cpfTf.getText()) && testa ==2){
            return 1;           
        }
        if(lR==null && testa==1){
           avisoRes.setText("Selecione o responsavel.");
        }
        if(end==null)
        {
            avisoEnd.setText("Selecione o endereco.");
        }
        if(tP==null)
        {
            avisoTp.setText("Selecione o tipo de pagamento.");
        }
      
        
        
        return 0;
    }       
    

    @FXML
    public int testaData() {
        int idade = 0;
        int x = 0;
        if (nascimento.getText().length() == 10) {
            aviso3.setText("");
            String s = nascimento.getText();
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            df.setLenient(false); // aqui o pulo do gato

            String[] aux = s.split("/");
            int dia = Integer.parseInt(aux[0]); // igual ao dia
            int mes = Integer.parseInt(aux[1]) - 1;// mes
            int ano = Integer.parseInt(aux[2]); //ano

            Calendar a = Calendar.getInstance();
            a.setTime(hoje);//data maior

            Calendar b = Calendar.getInstance();
            b.set(ano, mes, dia);// data menor

            a.add(Calendar.YEAR, -b.get(Calendar.YEAR));
            idade = (a.get(Calendar.YEAR));
            System.out.println(idade);
            if (idade < 18) {
                buscaRes.setDisable(false);
                listaRes.setDisable(false);
                buscaRes.setOpacity(1);
                listaRes.setOpacity(1);
                responsavel.setDisable(false);
                responsavelBt.setDisable(false);
                responsavel.setOpacity(1);
                responsavelBt.setOpacity(1);
                limp2();
                novo = new ClienteMenor();
                this.testa=1;
            } else {
                cpf.setDisable(false);
                cpf.setOpacity(1);
                cpfTf.setDisable(false);
                cpfTf.setOpacity(1);
                novo = new ClienteMaior();
                limp();
                this.testa=2;
            }
            try {
                df.parse(s);
            } catch (ParseException ex) {
                aviso3.setText("Data Inválida");
                x = 1;
            }

        } else {
            limp2();
            limp();
        }
        return x;
    }

    @FXML
    public void cadastrar(){
        if (testaCampo() == 0 && testaData() == 0) {
            novo.setNome(nCompleto.getText());
            novo.setTelefone(telefone.getText()); 
            SimpleDateFormat forma = new SimpleDateFormat("dd/MM/yy");
            try {
                novo.setDataNascimento(forma.parse(nascimento.getText()));
            } catch (ParseException ex) {
                System.out.println("data invalida");
            }
           novo.setDataCadastro(hoje);
            novo.setTipoPagamento(tP);
            novo.setEndereco(end);       
            
            if(testa==2){               
              novoMaior.setCpf(cpfTf.getText());
            }
            if(testa==1){
              novoMenor.setResponsavel((ClienteMaior) lR);                       
            }
           
            //if (iid.getText() != null) {                
                if(novo.update()){
                     
                    avisos.setText("Cliente atualizado");
                    limpaTexto();
                }
                else{
                    avisos.setText("Não foi possível atualizar o cliete.");
                }
            //} else {
                if (novo.insert()) {
                    avisos.setText("Cliente cadastrado com id " + novo.getId());
                    limpaTexto();
                } else {
                    avisos.setText("Não foi possível cadastrar o Cliente.");
                }
            //}
           
            
            
            
            
            
            
        } else {
            avisos.setText("Revise o preenchimento dos campos.");
        }
    }

    private void limpaTexto() {
        nCompleto.setText("Brasil");
        telefone.setText("");
        nascimento.setText("");
        clienteCompleto.setText("");
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
//        iid=null;    
        cpf.setDisable(true);
        cpf.setOpacity(0);
        cpfTf.setDisable(true);
        cpfTf.setOpacity(0);
        buscaRes.setDisable(true);
        listaRes.setDisable(true);
        buscaRes.setOpacity(0);
        listaRes.setOpacity(0);
        responsavel.setDisable(true);
        responsavelBt.setDisable(true);
        responsavel.setOpacity(0);
        responsavelBt.setOpacity(0);
        addItems3();
        addItems2();
        addItems1();
        addItems();
        if (pgProxima == null) {
            frente.setDisable(true);
        } else {
            frente.setDisable(false);
        }
        makeColumns();
//         iid.setText(null);
    }

    public void limp() {
        buscaRes.setDisable(true);
        listaRes.setDisable(true);
        buscaRes.setOpacity(0);
        listaRes.setOpacity(0);
        responsavel.setDisable(true);
        responsavelBt.setDisable(true);
        responsavel.setOpacity(0);
        responsavelBt.setOpacity(0);
    }

    public void limp2() {
        cpf.setDisable(true);
        cpfTf.setDisable(true);
        cpf.setOpacity(0);
        cpfTf.setOpacity(0);

    }

    @FXML
    private void addItems2() {
        try {
            listaEnd.getItems().clear();
            for (Endereco e : Endereco.getAll()) {
                if (buscaEnd.getText() == null || e.containsText((buscaEnd.getText()))) {
                listaEnd.getItems().add(e);
                }
            }
        } catch (Exception e) {
            System.out.println("");
        }
    }

    @FXML
    private void addItems3() {
        int idade2;
        try {
            
            listaRes.getItems().clear();
            for (Pessoa c : Cliente.getAll()) {                
                if (buscaRes.getText() == null || c.containsText((buscaRes.getText()))) {
                Date s = c.getDataNascimento();
                DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                df.setLenient(false); // aqui o pulo do gato

                Calendar a = Calendar.getInstance();
                a.setTime(hoje);//data maior

                Calendar b = Calendar.getInstance();
                b.setTime(s);// data menor

                a.add(Calendar.YEAR, -b.get(Calendar.YEAR));
                idade2 = (a.get(Calendar.YEAR));

                if (idade2 > 18) {
                    listaRes.getItems().add((Cliente) c);
                    
                }
                // }
            }
            }
        } catch (Exception e) {
            System.out.println("");
        }
    }

    @FXML
    private void sel2(MouseEvent event) {
        end = listaEnd.getSelectionModel().getSelectedItem();
        endereco.setText(end.enderecoCompleto());
    }

    @FXML
    public void voltar() {
        Main.trocaTela(pgAnterior);
    }

    @FXML
    public void trocaEndereco() {
        TelaEnderecoController.setPgAnterior(fxml);
        setPgProxima("TelaEndereco.fxml");
        Main.trocaTela(pgProxima);
    }

    @FXML
    public void trocaResponsavel() {
        TelaResponsavelController.setPgAnterior("TelaCliente.fxml");
        Main.trocaTela("TelaResponsavel.fxml");
    }

    @FXML
    private void frente(ActionEvent event
    ) {
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void menu(ActionEvent event
    ) {
        Main.trocaTela(TelaInicialController.getMenu());
    }

    @FXML
    private void trocaCliente(ActionEvent event
    ) {
        //nada
    }

    @FXML
    private void trocaViagem(ActionEvent event
    ) {
        TelaViagensController.setPgAnterior(fxml);
        setPgProxima("TelaViagens.fxml");
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void trocaMotorista(ActionEvent event
    ) {
        TelaMotoristaController.setPgAnterior(fxml);
        setPgProxima("TelaMotorista.fxml");
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void trocaVeiculo(ActionEvent event
    ) {
        TelaVeiculoController.setPgAnterior(fxml);
        setPgProxima("TelaVeiculo.fxml");
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void trocaTrajeto(ActionEvent event
    ) {
        TelaTrajetoController.setPgAnterior(fxml);
        setPgProxima("TelaTrajeto.fxml");
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void trocaUsuario(ActionEvent event
    ) {
        Main.trocaTela("TelaInicial.fxml");
    }

    @FXML
    private void trocaTVeiculo(ActionEvent event
    ) {
        TelaTipoVeiculosController.setPgAnterior(fxml);
        setPgProxima("TelaTipoVeiculos.fxml");
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void trocaTCombustivel(ActionEvent event
    ) {
        TelaTipoCombustivelController.setPgAnterior(fxml);
        setPgProxima("TelaTipoCombustiveis.fxml");
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void trocaTPagamento(ActionEvent event
    ) {
        TelaPagamentoController.setPgAnterior(fxml);
        setPgProxima("TelaPagamento.fxml");
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void trocaTFrequencia(ActionEvent event
    ) {
        TelaFrequenciasController.setPgAnterior(fxml);
        setPgProxima("TelaFrequencias.fxml");
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void sair(ActionEvent event
    ) {
        Main.trocaTela("TelaInicial.fxml");
    }

    private void credito() {
        boleto.showingProperty();
    }

    private List<String> lista = new ArrayList();
    private ObservableList<ArrayList> observableListPagamentos;

    @FXML
    private void addItems1() {
        try {
            listaTP.getItems().clear();
            for (TipoPagamento t : TipoPagamento.getAll()) {
                if (buscaTP.getText() == null || t.getDescricao().toLowerCase().contains((buscaTP.getText().toLowerCase()))) {
                listaTP.getItems().add(t);
                System.out.println();
                 }
            }
        } catch (Exception e) {
            System.out.println("");
        }
    }

    @FXML
    private void sel1(MouseEvent event) {
        tP = listaTP.getSelectionModel().getSelectedItem();
        tipoPagamento.setText(tP.getDescricao());
    }

    @FXML
    private void sel3(MouseEvent event) {
        lR = listaRes.getSelectionModel().getSelectedItem();
        responsavelLb.setText(lR.toString());
    }
@FXML
    private void selecionarCliente(MouseEvent event) {
        novo = (Cliente) tabela.getSelectionModel().getSelectedItem();
        System.out.println(novo.toString() + novo.getId());
        clienteCompleto.setText(novo.clienteCompleto());
        editarButton.setDisable(false);
        excluirButton.setDisable(false);

    }

    @FXML
    private void editar(ActionEvent event) {
        System.out.println(novo.toString() + novo.getId());
       // 
       
        //this.iid.setText(novo.getId()+"");
        nCompleto.setText(novo.getNome() + "");
        telefone.setText(novo.getTelefone() + "");
        nascimento.setText(novo.getDataNFormatada());
        tipoPagamento.setText(novo.getTipoPagamento().getDescricao());
        endereco.setText(novo.getEndereco().enderecoCompleto());
        tabs.getSelectionModel().select(tabNovo);
        testaData();
        try{
            cpfTf.setText(((ClienteMaior)novo).getCpf() + "");
        }
        catch(Exception e)
        {
            responsavelLb.setText(((ClienteMenor)novo).getResponsavel().clienteCompleto());
        }
    }

    private void makeColumns() {
        nomeT.setCellValueFactory(new PropertyValueFactory<>("nome"));
        numeroT.setCellValueFactory(new PropertyValueFactory<>("telefone"));
        nascimentoT.setCellValueFactory(new PropertyValueFactory<>("dataNascimento"));
        pagamentoT.setCellValueFactory(new PropertyValueFactory<>("tipoPagamento"));
    }
    
@FXML
    private void excluir(ActionEvent event) {
        if (!novo.delete()) {
            avisos2.setText("Cliente em uso. Exclusão não efetuada.");
        }
        else
        {
            avisos2.setText("Cliente excluído.");
            addItems();
        }
    }


    @FXML
    private void addItems() {
        try {
            tabela.getItems().clear();
            for (Pessoa c : Cliente.getAll()) {
                if (busca.getText() == null || c.containsText((busca.getText()))) {
                tabela.getItems().add((Cliente) c);
                System.out.println();
                // }
            }
            }
        } catch (Exception e) {
            System.out.println("");
        }
    }

}
