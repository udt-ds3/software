/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import model.Main;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Aluno
 */
public class TelaInicialController implements Initializable {

    public static String menu = "TelaInicial.fxml";

    public static String getMenu() {
        return menu;
    }

    public static void setMenu(String menu) {
        TelaInicialController.menu = menu;
    }
  
    @FXML
    private Label aviso;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    public void voltar(){
        System.exit(0);
    }
    
    @FXML
    public void logar(){
            TelaInicialController.setMenu("TelaFuncionario.fxml");
            Main.trocaTela("TelaFuncionario.fxml");
        
    }
    
}
