/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import model.Main;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import model.TipoCombustivel;
import model.TipoVeiculo;
import model.Veiculo;

/**
 * FXML Controller class
 *
 * @author Aluno
 */
public class TelaVeiculoController implements Initializable {

    /**
     * Initializes the controller class.
     */
    private static String pgProxima = null;

    public static void setPgProxima(String pgProxima) {
        TelaVeiculoController.pgProxima = pgProxima;
    }
    private Veiculo novo = new Veiculo();
    private TipoVeiculo tV = null;
    private TipoCombustivel tC =null;
    
    private String fxml = "TelaVeiculo.fxml";
    private static String pgAnterior = "TelaAdm.fxml";

    public static void setPgAnterior(String pgAnterior) {
        TelaVeiculoController.pgAnterior = pgAnterior;
        TelaVeiculoController.pgProxima = null;
    }
    @FXML
    private Button volta;
    @FXML
    private Button frente;
    @FXML
    private MenuItem usuarios;
    @FXML
    private TextField placa;
    @FXML
    private TextField renavam;
    @FXML
    private TextField anoFab;
    @FXML
    private TextField chassi;
    @FXML
    private TextField lugares;
    @FXML
    private TextField quilometragem;
    @FXML
    private TextField buscaTC;
    @FXML
    private TextField buscaTV;
    @FXML
    private Label tipoVeiculo;
    @FXML
    private Label tipoCombustivel;
    @FXML
    private ListView<TipoVeiculo> listaTV;
    @FXML
    private ListView<TipoCombustivel> listaTC;
    @FXML
    private Label avisoTV;
    @FXML
    private Label avisoTC;
    @FXML
    private Label aviso;
    @FXML
    private ListView<Veiculo> lista;
    @FXML
    private TabPane tabs;
    @FXML
    private Tab tabNovo;
    @FXML
    private Tab tabLista;
    @FXML
    private Label avisos;
    @FXML
    private TextField busca;
    @FXML
    private Tab tadDados;
    @FXML
    private TableView<Veiculo> tabelaDados;
    @FXML
    private TableColumn<?, ?> tipoT;
    @FXML
    private TableColumn<?, ?> numT;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        addItems();
        addItems1();
        addItems2();
        makeColumns();
        if (pgProxima == null) {
            frente.setDisable(true);
        } else {
            frente.setDisable(false);
        }
    }

    @FXML
    public void voltar() {
        Main.trocaTela(pgAnterior);
    }

    @FXML
    public void trocaEndereco() {
        TelaEnderecoController.setPgAnterior(fxml);
        setPgProxima("TelaEndereco.fxml");
        Main.trocaTela(pgProxima);
    }

    public void trocaResponsavel() {
        TelaResponsavelController.setPgAnterior("TelaCliente.fxml");
        Main.trocaTela("TelaResponsavel.fxml");
    }

    @FXML
    private void frente(ActionEvent event) {
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void menu(ActionEvent event) {
        Main.trocaTela(TelaInicialController.getMenu());
    }

    @FXML
    private void trocaCliente(ActionEvent event) {
        TelaClienteController.setPgAnterior(fxml);
        setPgProxima("TelaCliente.fxml");
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void trocaViagem(ActionEvent event) {
        TelaViagensController.setPgAnterior(fxml);
        setPgProxima("TelaViagens.fxml");
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void trocaMotorista(ActionEvent event) {
        TelaMotoristaController.setPgAnterior(fxml);
        setPgProxima("TelaMotorista.fxml");
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void trocaVeiculo(ActionEvent event) {
        //nada
    }

    @FXML
    private void trocaTrajeto(ActionEvent event) {
        TelaTrajetoController.setPgAnterior(fxml);
        setPgProxima("TelaTrajeto.fxml");
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void trocaUsuario(ActionEvent event) {
        Main.trocaTela("TelaInicial.fxml");
    }

    @FXML
    private void trocaTVeiculo(ActionEvent event) {
        TelaTipoVeiculosController.setPgAnterior(fxml);
        setPgProxima("TelaTipoVeiculos.fxml");
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void trocaTCombustivel(ActionEvent event) {
        TelaTipoCombustivelController.setPgAnterior(fxml);
        setPgProxima("TelaTipoCombustiveis.fxml");
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void trocaTPagamento(ActionEvent event) {
        TelaPagamentoController.setPgAnterior(fxml);
        setPgProxima("TelaPagamento.fxml");
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void trocaTFrequencia(ActionEvent event) {
        TelaFrequenciasController.setPgAnterior(fxml);
        setPgProxima("TelaFrequencias.fxml");
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void sair(ActionEvent event) {
        Main.trocaTela("TelaInicial.fxml");
    }

    @FXML
    private void salvar(ActionEvent event) {
        if(testaCampo())
        {
            try{
            novo.setAnoFab(Integer.parseInt(anoFab.getText()));
            novo.setNumChassi(chassi.getText());
            novo.setPlaca(placa.getText());
            novo.setQuantLug(Integer.parseInt(lugares.getText()));
            novo.setQuilometragem(Integer.parseInt(quilometragem.getText()));
            novo.setRenavam(Long.parseLong(renavam.getText()));
            novo.setTipoComb(tC);
            novo.setTipoVeiculo(tV);
            
            if(novo.update())
            {
                aviso.setText("Veículo atualizado.");
            }
            else
            {
                if(novo.insert())
                {
                    aviso.setText("Veículo cadastrado.");
                }
                else
                {
                    aviso.setText("Não foi possível salvar.");
                }
            }
            }
            catch(NumberFormatException e)
            {
                aviso.setText("Revise os campos.");
            }
        }
        else
        {
            aviso.setText("Revise os campos.");
        }
    }

    private boolean testaCampo()
    {
        if(tC==null)
        {
            avisoTC.setText("Selecione o tipo de combustivel.");
        }
        if(tV==null)
        {
            avisoTV.setText("Selecione o tipo de veículo.");
        }
        if(renavam.getText()==null || "".equals(renavam.getText()))
        {
            return false;
        }
        if(placa.getText()==null || "".equals(placa.getText()))
        {
            return false;
        }
        if(chassi.getText()==null || "".equals(chassi.getText()))
        {
            return false;
        }
        if(anoFab.getText()==null || "".equals(anoFab.getText()))
        {
            return false;
        }
        if(quilometragem.getText()==null || quilometragem.getText()=="")
        {
            return false;
        }
        if(lugares.getText()==null || lugares.getText()=="")
        {
            return false;
        }
        if(tipoVeiculo.getText()=="")
        {
            return false;
        }
        if(tipoCombustivel.getText()=="")
        {
            return false;
        }
        
        return true;
    }
    @FXML
    private void limpaTexto(ActionEvent event) {
        renavam.setText("");
        placa.setText("");
        chassi.setText("");
        anoFab.setText("");
        quilometragem.setText("");
        lugares.setText("");
        tipoVeiculo.setText("");
        tC = null;
        tV = null;
        tipoCombustivel.setText("");
        novo = new Veiculo();
    }
    @FXML
    private void limpaCampo() {
        avisos.setText("");
        aviso.setText("");
        avisoTV.setText("");
        avisoTC.setText("");
    }

    @FXML
    private void selecionar(MouseEvent event) {
        novo = lista.getSelectionModel().getSelectedItem();
    }

    @FXML
    private void editar(ActionEvent event) {
        renavam.setText(""+novo.getRenavam());
        placa.setText(""+novo.getPlaca());
        chassi.setText(""+novo.getNumChassi());
        anoFab.setText(""+novo.getAnoFab());
        quilometragem.setText(""+novo.getQuilometragem());
        lugares.setText(""+novo.getQuantLug());
        tipoVeiculo.setText(""+novo.getTipoVeiculo().getDescricao());
        tipoCombustivel.setText(""+novo.getTipoComb().getDescricao());
        tC = novo.getTipoComb();
        tV = novo.getTipoVeiculo();
        tabs.getSelectionModel().select(tabNovo);
    }

    @FXML
    private void excluir(ActionEvent event) {
        if (!novo.delete()) {
            avisos.setText("Veículo em uso. Exclusão não efetuada.");
        }
        else
        {
            avisos.setText("Veículo excluído.");
            addItems();
        }
    }

    @FXML
    private void sel1(MouseEvent event) {
       tV = listaTV.getSelectionModel().getSelectedItem();
       tipoVeiculo.setText(tV.getDescricao());
    }

    @FXML
    private void addItems() {
        try {
            lista.getItems().clear();
            for (Veiculo v : Veiculo.getAll()) {
                if (busca.getText() == null|| v.toString().toLowerCase().contains((busca.getText().toLowerCase()))) {
                    lista.getItems().add(v);
                }
            }
        } catch (Exception e) {
            System.out.println("");
        }
    }
    
    @FXML
    private void addItems1() {
        try {
            listaTV.getItems().clear();
            for (TipoVeiculo t : TipoVeiculo.getAll()) {
                if (buscaTV.getText() == null || t.getDescricao().toLowerCase().contains((buscaTV.getText().toLowerCase()))) {
                    listaTV.getItems().add(t);
                }
            }
        } catch (Exception e) {
            System.out.println("");
        }
    }
    @FXML
    private void addItems2() {
        try {
            listaTC.getItems().clear();
            for (TipoCombustivel t : TipoCombustivel.getAll()) {
                if (buscaTC.getText() == null || t.getDescricao().toLowerCase().contains((buscaTC.getText().toLowerCase()))) {
                    listaTC.getItems().add(t);
                }
            }
        } catch (Exception e) {
            System.out.println("");
        }
    }
    
    @FXML
    private void sel2(MouseEvent event) {
        tC = listaTC.getSelectionModel().getSelectedItem();
        tipoCombustivel.setText(tC.getDescricao());
    }

    private void makeColumns() {
        numT.setCellValueFactory(new PropertyValueFactory<>("quantLug"));
        tipoT.setCellValueFactory(new PropertyValueFactory<>("numChassi"));
    }
    
    @FXML
    private void byTV(ActionEvent event) {
        try {
            tabelaDados.getItems().clear();
            for (Veiculo v : Veiculo.getGroupBy("numtipov")) {
                tabelaDados.getItems().add(v);
            }
        } catch (Exception e) {
            System.out.println("");
        }
    }

    @FXML
    private void byTC(ActionEvent event) {
        try {
            tabelaDados.getItems().clear();
            for (Veiculo v : Veiculo.getGroupBy("numtipoc")) {
                tabelaDados.getItems().add(v);
            }
        } catch (Exception e) {
            System.out.println("");
        }
    }
}
