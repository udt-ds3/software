/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import model.Main;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Aluno
 */
public class TelaMotoristaController implements Initializable {
    private static String pgProxima = null;
    private String fxml="TelaMotorista.fxml";
    public static void setPgProxima(String pgProxima) {
        TelaMotoristaController.pgProxima = pgProxima;
    }
    private static String pgAnterior = "TelaAdm.fxml";

    public static void setPgAnterior(String pgAnterior) {
        TelaMotoristaController.pgAnterior = pgAnterior;
        TelaMotoristaController.pgProxima = null;
    }
    
    @FXML
    private Label textNome;
    @FXML
    private Label textTelefone;
    @FXML
    private Label textNumeroHabilitacao;
    @FXML
    private Label textCategoria;
    @FXML
    private Label textData;
    @FXML
    private Label textVencimento;
    @FXML
    private TextField nomeTf;
    @FXML
    private TextField telefoneTf;
    @FXML
    private TextField nmHabilitacaoTf;
    @FXML
    private TextField categoriaTf;
    @FXML
    private TextField vencimentoTf;
    @FXML
    private TextField dtHabilitacaoTf;
    @FXML
    private Button volta;
    @FXML
    private Button frente;
    @FXML
    private Button frente1;
    @FXML
    private MenuItem usuarios;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        if(pgProxima==null)
        {
            frente.setDisable(true);
        }
        else
        {
            frente.setDisable(false);
        }
    }

    @FXML
    public void limpaCampo() {
        textNome.setText("");
        textTelefone.setText("");
        textNumeroHabilitacao.setText("");
        textCategoria.setText("");
        textVencimento.setText("");
        textData.setText("");
    }

    public int testaCampo() {
        int x = 0;
        if ("".equals(nomeTf.getText())) {
            textNome.setText("CAMPO VAZIO!");
            x = 1;
        }
        if ("".equals(telefoneTf.getText())) {
            textTelefone.setText("CAMPO VAZIO!");
            x = 1;
        }
        if ("".equals(nmHabilitacaoTf.getText())) {
            textNumeroHabilitacao.setText("CAMPO VAZIO!");
            x = 1;
        }
        if ("".equals(categoriaTf.getText())) {
            textCategoria.setText("CAMPO VAZIO!");
            x = 1;
        }
        if ("".equals(vencimentoTf.getText())) {
            textVencimento.setText("CAMPO VAZIO!");
            x = 1;
        }
        if ("".equals(dtHabilitacaoTf.getText())) {
            textData.setText("CAMPO VAZIO!");
            x = 1;
        }
        return x;
    }

    @FXML
    public void cadastrar() throws IOException {
        if (testaCampo() == 0) {
            Main.trocaTela("TelaAdm.fxml");
        }

    }
    
    @FXML
    public void voltar(){
        Main.trocaTela(pgAnterior);
    }

    @FXML
    public void trocaEndereco()  {
         TelaEnderecoController.setPgAnterior(fxml);
         setPgProxima("TelaEndereco.fxml");
         Main.trocaTela(pgProxima);
    }
    
    @FXML
    public void trocaResponsavel()  {
        TelaResponsavelController.setPgAnterior("TelaCliente.fxml");
        Main.trocaTela("TelaResponsavel.fxml");
    }
   

    @FXML
    private void frente(ActionEvent event) {
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void menu(ActionEvent event) {
        Main.trocaTela(TelaInicialController.getMenu());
    }

    @FXML
    private void trocaCliente(ActionEvent event) {
        TelaClienteController.setPgAnterior(fxml);
        setPgProxima("TelaCliente.fxml");
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void trocaViagem(ActionEvent event) {
        TelaViagensController.setPgAnterior(fxml);
        setPgProxima("TelaViagens.fxml");
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void trocaMotorista(ActionEvent event) {
        //nada
    }

    @FXML
    private void trocaVeiculo(ActionEvent event) {
        TelaVeiculoController.setPgAnterior(fxml);
        setPgProxima("TelaVeiculo.fxml");
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void trocaTrajeto(ActionEvent event) {
        TelaTrajetoController.setPgAnterior(fxml);
        setPgProxima("TelaTrajeto.fxml");
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void trocaUsuario(ActionEvent event) {
        Main.trocaTela("TelaInicial.fxml");
    }
    
    @FXML
    private void trocaTVeiculo(ActionEvent event) {
        TelaTipoVeiculosController.setPgAnterior(fxml);
        setPgProxima("TelaTipoVeiculos.fxml");
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void trocaTCombustivel(ActionEvent event) {
        TelaTipoCombustivelController.setPgAnterior(fxml);
        setPgProxima("TelaTipoCombustiveis.fxml");
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void trocaTPagamento(ActionEvent event) {
        TelaPagamentoController.setPgAnterior(fxml);
        setPgProxima("TelaPagamento.fxml");
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void trocaTFrequencia(ActionEvent event) {
        TelaFrequenciasController.setPgAnterior(fxml);
        setPgProxima("TelaFrequencias.fxml");
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void sair(ActionEvent event) {
        Main.trocaTela("TelaInicial.fxml");
    }
}