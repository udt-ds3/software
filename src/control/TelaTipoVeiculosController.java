/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import model.Main;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import model.TipoVeiculo;

/**
 * FXML Controller class
 *
 * @author Aluno
 */
public class TelaTipoVeiculosController implements Initializable {
    private String fxml="TelaTipoVeiculos.fxml";
    TipoVeiculo novo = new TipoVeiculo();
    private static String pgProxima = null;
    public static void setPgProxima(String pgProxima) {
        TelaTipoVeiculosController.pgProxima = pgProxima;
    }
   private static String pgAnterior = "TelaAdm.fxml";

    public static void setPgAnterior(String pgAnterior) {
        TelaTipoVeiculosController.pgAnterior = pgAnterior;
        TelaTipoVeiculosController.pgProxima = null;
    }
    @FXML
    private Button volta;
    @FXML
    private Button frente;
    @FXML
    private MenuItem usuarios;
    @FXML
    private TextField id;
    @FXML
    private TableView<TipoVeiculo> tabela;
    @FXML
    private TableColumn<?, ?> idT;
    @FXML
    private TableColumn<?, ?> descT;
    @FXML
    private Label avisos;
    @FXML
    private TextField descricao;
    @FXML
    private TextField busca;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {        
        id.setText(null);
        makeColumns();
        addItems();
        if(pgProxima==null)
        {
            frente.setDisable(true);
        }
        else
        {
            frente.setDisable(false);
        }
    }    
    
    @FXML
    public void voltar(){
        Main.trocaTela(pgAnterior);
    }

    private void makeColumns() {
        idT.setCellValueFactory(new PropertyValueFactory<>("id"));
        descT.setCellValueFactory(new PropertyValueFactory<>("descricao"));
    }
    
    @FXML
    public void trocaEndereco()  {
         TelaEnderecoController.setPgAnterior(fxml);
         setPgProxima("TelaEndereco.fxml");
         Main.trocaTela(pgProxima);
    }
    
    public void trocaResponsavel()  {
        TelaResponsavelController.setPgAnterior("TelaCliente.fxml");
        Main.trocaTela("TelaResponsavel.fxml");
    }
   

    @FXML
    private void frente(ActionEvent event) {
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void menu(ActionEvent event) {
        Main.trocaTela(TelaInicialController.getMenu());
    }

    @FXML
    private void trocaCliente(ActionEvent event) {
        TelaClienteController.setPgAnterior(fxml);
        setPgProxima("TelaCliente.fxml");
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void trocaViagem(ActionEvent event) {
        TelaViagensController.setPgAnterior(fxml);
        setPgProxima("TelaViagens.fxml");
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void trocaMotorista(ActionEvent event) {
        TelaMotoristaController.setPgAnterior(fxml);
        setPgProxima("TelaMotorista.fxml");
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void trocaVeiculo(ActionEvent event) {
        TelaVeiculoController.setPgAnterior(fxml);
        setPgProxima("TelaVeiculo.fxml");
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void trocaTrajeto(ActionEvent event) {
        TelaTrajetoController.setPgAnterior(fxml);
        setPgProxima("TelaTrajeto.fxml");
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void trocaUsuario(ActionEvent event) {
        Main.trocaTela("TelaInicial.fxml");
    }

    @FXML
    private void trocaTVeiculo(ActionEvent event) {
        //nada
    }

    @FXML
    private void trocaTCombustivel(ActionEvent event) {
        TelaTipoCombustivelController.setPgAnterior(fxml);
        setPgProxima("TelaTipoCombustiveis.fxml");
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void trocaTPagamento(ActionEvent event) {
        TelaPagamentoController.setPgAnterior(fxml);
        setPgProxima("TelaPagamento.fxml");
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void trocaTFrequencia(ActionEvent event) {
        TelaFrequenciasController.setPgAnterior(fxml);
        setPgProxima("TelaFrequencias.fxml");
        Main.trocaTela(pgProxima);
    }

    @FXML
    private void sair(ActionEvent event) {
        Main.trocaTela("TelaInicial.fxml");
    }


    @FXML
    private void addItems() {
        try {
            tabela.getItems().clear();
            for (TipoVeiculo t : TipoVeiculo.getAll()) {
                if (busca.getText() == null || t.getDescricao().toLowerCase().contains((busca.getText().toLowerCase()))) {
                    tabela.getItems().add(t);
                }
            }
        } catch (Exception e) {
            System.out.println("");
        }
    }

    @FXML
    private void limpaCampos() {
        avisos.setText("");
    }
    @FXML
    private void limpaTexto() {
        id.setText(null);
        descricao.setText("");
    }
    

    @FXML
    private void selecionar(MouseEvent event) {
        novo = tabela.getSelectionModel().getSelectedItem();
    }

    @FXML
    private void editar(ActionEvent event) {
        id.setText(novo.getId()+"");
        descricao.setText(novo.getDescricao());
    }

    @FXML
    private void excluir(ActionEvent event) {
        if (!novo.delete()) {
            avisos.setText("Tipo em uso. Exclusão não efetuada.");
        }
        else
        {
            avisos.setText("Tipo excluído.");
            addItems();
        }
    }

    @FXML
    private void salvar(ActionEvent event) {
        if (descricao.getText()!=null && descricao.getText()!="") {
            novo.setDescricao(descricao.getText());
            if (id.getText() != null) {
                if(novo.update()){
                    avisos.setText("Tipo de veículo atualizado");
                    limpaTexto();
                }
                else{
                    avisos.setText("Não foi possível atualizar o tipo de veículo.");
                }
            } else {
                if (novo.insert()) {
                    avisos.setText("Tipo de veículo cadastrado com id " + novo.getId());
                    limpaTexto();
                } else {
                    avisos.setText("Não foi possível cadastrar o tipo de veículo.");
                }
            }

            addItems();
        } else {
            avisos.setText("Revise o preenchimento dos campos.");
        }

    }
}
