/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;

/**
 *
 * @author Aluno
 */
public class Viagem {
    private int id;
    private Motorista motorista;
    private Veiculo veiculo;
    private FreqViagem frequencia;
    private ArrayList<Endereco> enderecos = new ArrayList();
    private ArrayList<Trajeto> trajetos = new ArrayList();
    private ArrayList<Cliente> cliente = new ArrayList();
    
    public static ArrayList<Viagem> getAll()
    {
       
        return new ArrayList();
    }
    
    public static Viagem getById(int id)
    {
        
        return new Viagem();
    }
    
    public void insert()
    {
        
    }
    
    public void update()
    {
        
    }
    
    public void getThis()
    {
        
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Motorista getMotorista() {
        return motorista;
    }

    public void setMotorista(Motorista motorista) {
        this.motorista = motorista;
    }

    public Veiculo getVeiculo() {
        return veiculo;
    }

    public void setVeiculo(Veiculo veiculo) {
        this.veiculo = veiculo;
    }

    public FreqViagem getFrequencia() {
        return frequencia;
    }

    public void setFrequencia(FreqViagem frequencia) {
        this.frequencia = frequencia;
    }

    public ArrayList<Endereco> getEnderecos() {
        return enderecos;
    }

    public void setEnderecos(ArrayList<Endereco> enderecos) {
        this.enderecos = enderecos;
    }

}