
package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class TipoPagamento extends Tipo{
    
    public static ArrayList<TipoPagamento> getAll()
    {
        ArrayList<TipoPagamento> all = new ArrayList();

        
        Connection dbConnection = Conexao.getConexao();
        Statement st;
        String select = "Select * From TipoPagamento";
        TipoPagamento t = new TipoPagamento();
        try {
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(select);

            while (rs.next()) {
                t.setId(rs.getInt("numTipo"));
                t.setDescricao(rs.getString("descricao"));
                System.out.println(rs.getString("descricao"));
                all.add(t);
                t = new TipoPagamento();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return all;
        
    }
    
    public static TipoPagamento getById(int id)
    {
        Connection dbConnection = Conexao.getConexao();
        Statement st;
        TipoPagamento t = new TipoPagamento();
        
        String select = "Select * From TipoPagamento where numtipo = "+id;
        try {
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(select);

            if(rs.next()) {
                t.setDescricao(rs.getString("descricao"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return t;
    }
    
    public boolean insert()
    {
        String generatedColumns[] = {"numTipo"};
        
        Connection dbConnection = Conexao.getConexao();
        PreparedStatement ps = null;

        String insert = "insert into TipoPagamento (numtipo, descricao) values (idtpagamento.nextval, ?)";
        try {
            ps = dbConnection.prepareStatement(insert, generatedColumns);
            ps.setString(1, this.getDescricao());
            ps.execute();
            ResultSet rs = ps.getGeneratedKeys();

            if (rs.next()) {
                setId(rs.getInt(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            
            return false;
        }
        
        return true;
    }
    
    public boolean update()
    {
        Connection dbConnection = Conexao.getConexao();
        PreparedStatement ps = null;

        String update = "update TipoPagamento set descricao = ? where (numtipo = ?)";
        try {
            ps = dbConnection.prepareStatement(update);
            ps.setString(1, this.getDescricao());
            ps.setInt(2, this.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            
            return false;
        }
        
        return true;   
    }
    
    public void getThis()
    {
         Connection dbConnection = Conexao.getConexao();
        Statement st;
        String select = "Select * From TipoPagamento where numtipo = "+getId();
        try {
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(select);

            if(rs.next()) {
                setDescricao(rs.getString("descricao"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }  
    }
    
    public boolean delete()
    {
        Connection dbConnection = Conexao.getConexao();
        Statement st;
        String select = "delete from TipoPagamento where numTipo = " + getId();

        try {
            st = dbConnection.createStatement();
            st.executeQuery(select);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
