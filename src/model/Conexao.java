package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Aluno
 */

public class Conexao {

    private static String usuario = "udt";
    private static String senha = "udt123";
    private static String servidor = "oracle.canoas.ifrs.edu.br";
    private static int porta = 1521;

    private static Connection conexao = null;

    public Conexao() {
    }//inicia com os valores padrões

    public Conexao(
            String usuario,
            String senha) {
        Conexao.senha = senha;
        Conexao.usuario = usuario;
    }

    public static Connection getConexao() {
        if (conexao==null) {
            try {
                Class.forName("oracle.jdbc.driver.OracleDriver");
                conexao = DriverManager.getConnection(
                        "jdbc:oracle:thin:@" + Conexao.servidor + ":" + Conexao.porta + ":XE",
                        Conexao.usuario,
                        Conexao.senha);

            } catch (ClassNotFoundException e) {
                System.out.println("Senhor programador! Importe o pacote do DB antes de xingar o java");
            } catch (Exception e) {
                e.printStackTrace(); //Sei lá que diabos tu fez então olhe com calma as coisas.
            }

        }
        return conexao;
    }

    public static void desconecta() {
        try {
            conexao.close();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
