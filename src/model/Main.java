/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author D. Millan
 */
public class Main extends Application {  
    private static Stage stage;

    public static Stage getStage() {
        return stage;
    }
    
    public static void trocaTela(String tela){
        Parent root = null;
        try{
            root = FXMLLoader.load(Main.class.getResource("/control/"+tela));
        }catch(Exception e){
            System.out.println("Verificar arquivo FXML");
        }
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.show();
    }
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/control/TelaInicial.fxml"));
        
        Scene scene = new Scene(root);
        //observar essa linha = 
        Main.stage = stage;
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
        Conexao.desconecta();
    }
    
}

