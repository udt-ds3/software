
package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

public class Motorista extends Pessoa{
    private Date vencHab, dataHab;
    private String numHab;
    private char categoria;
    
   public static ArrayList<Pessoa> getAll(){
        ArrayList<Pessoa> all = new ArrayList();

        Connection dbConnection = Conexao.getConexao();
        Statement st;
        String select = "select * from Motorista inner join pessoa on pessoa.idp = Motorista.id";
        Motorista m = new Motorista();
        try {
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(select);

            while (rs.next()) {
                m.setCategoria(rs.getString("categoria").charAt(0));
                m.setNumHab(""+rs.getInt("numhab"));
                m.setDataHab(rs.getDate("Datahab"));
                m.setVencHab(rs.getDate("venchab"));
                m.setDataCadastro(rs.getDate("datacadastro"));
                m.setDataNascimento(rs.getDate("datanascimento"));
                m.setId(rs.getInt("idp"));
                m.setNome(rs.getString("nome"));
                m.setTelefone(rs.getInt("Telefone")+"");
                all.add(m);
                m = new Motorista();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return all;
    }
    
    public static Motorista getById(int id){
        Connection dbConnection = Conexao.getConexao();
        Statement st;
        String select = "select * from Motorista inner join pessoa on pessoa.idp = Motorista.id where id=" + id;
        Motorista m = new Motorista();
        try {
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(select);

                m.setCategoria(rs.getString("categoria").charAt(0));
                m.setNumHab(""+rs.getInt("numhab"));
                m.setDataHab(rs.getDate("Datahab"));
                m.setVencHab(rs.getDate("venchab"));
                m.setDataCadastro(rs.getDate("datacadastro"));
                m.setDataNascimento(rs.getDate("datanascimento"));
                m.setId(rs.getInt("idp"));
                m.setNome(rs.getString("nome"));
                m.setTelefone(rs.getInt("Telefone")+"");
                
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return m;
    }
    
    @Override
    public boolean insert(){
        super.insert();
        
        Connection dbConnection = Conexao.getConexao();
        PreparedStatement ps = null;

        String insert = "insert into motorista (venchab, datahab, numhab, categoria) values (?, ?, ?, ?)";
        try {
            ps = dbConnection.prepareStatement(insert);
            ps.setDate(1, (java.sql.Date) this.vencHab);
            ps.setDate(2, (java.sql.Date) this.dataHab);
            ps.setInt(3, Integer.parseInt(this.numHab));
            ps.setString(4, this.categoria+"");
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
            
            return false;
        }
        return true;
    }
    
    @Override
    public boolean update(){
       super.update();
        
       Connection dbConnection = Conexao.getConexao();
       PreparedStatement ps = null;

       String update = "update Motorista set  venchab = ?, datahab = ?, numhab = ?, categoria = ? where (id = ?)";
       try {
            ps = dbConnection.prepareStatement(update);
            ps.setDate(1, (java.sql.Date) this.vencHab);
            ps.setDate(2, (java.sql.Date) this.dataHab);
            ps.setInt(3, Integer.parseInt(this.numHab));
            ps.setString(4, this.categoria+"");
            ps.executeUpdate();
            
       } catch (SQLException e) {
            System.out.println(e.getMessage());
            return false;
       }
        
       return true;
    }
    
    @Override
    public boolean delete(){
        Connection dbConnection = Conexao.getConexao();
        Statement st;
        String select = "delete from motorista where id =" + this.getId();

        try{
            st = dbConnection.createStatement();
            st.executeQuery(select);
            
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        
        return true;
    }
    
    @Override
    public void getThis(){
        super.getThis();
        
        Connection dbConnection = Conexao.getConexao();
        Statement st;
        String select = "select * from motorista where id=" + this.getId();
        try {
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(select);
            this.setCategoria(rs.getString("categoria").charAt(0));
            this.setNumHab(""+rs.getInt("numhab"));
            this.setDataHab(rs.getDate("Datahab"));
            this.setVencHab(rs.getDate("venchab"));
        } catch (SQLException e) {
            e.printStackTrace();
        }  
        
    }

    public Date getVencHab() {
        return vencHab;
    }

    public void setVencHab(Date vencHab) {
        this.vencHab = vencHab;
    }

    public Date getDataHab() {
        return dataHab;
    }

    public void setDataHab(Date dataHab) {
        this.dataHab = dataHab;
    }

    public String getNumHab() {
        return numHab;
    }

    public void setNumHab(String numHab) {
        this.numHab = numHab;
    }

    public char getCategoria() {
        return categoria;
    }

    public void setCategoria(char categoria) {
        this.categoria = categoria;
    }
    
}
