
package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;

public class Veiculo {
    private String placa, numChassi;
    private Long renavam;
    private int anoFab, quilometragem, quantLug;
    private TipoVeiculo tipoVeiculo = new TipoVeiculo();
    private TipoCombustivel tipoComb = new TipoCombustivel();
    
    @Override
    public String toString() {
        return "Placa: " + placa + ", chassi: " + numChassi + ", renavam: " + renavam + ", " + anoFab + ", " + quilometragem + "km, " + quantLug + " lugares, " + tipoVeiculo.getDescricao() + ", " + tipoComb.getDescricao();
    }
    
    public static ArrayList<Veiculo> getAll()
    {
       ArrayList<Veiculo> all = new ArrayList();

        
        Connection dbConnection = Conexao.getConexao();
        Statement st;
        String select = "Select * From veiculo";
        Veiculo v = new Veiculo();
        try {
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(select);

            while (rs.next()) {
                v.anoFab = rs.getInt("anofab");
                v.numChassi = rs.getString("numchassi");
                v.placa = rs.getString("placa");
                v.quantLug = rs.getInt("quantlug");
                v.quilometragem = rs.getInt("quilometragem");
                v.renavam = rs.getLong("renavam");
                v.tipoComb.setId(rs.getInt("numtipoc"));
                v.tipoVeiculo.setId(rs.getInt("numtipov"));
                v.tipoComb.getThis();
                v.tipoVeiculo.getThis();
                all.add(v);
                v = new Veiculo();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return all;
    }
    
    public static Veiculo getById(int id)
    {
        return new Veiculo();
    }
    
        public static ArrayList<Veiculo> getGroupBy(String campo) {
        
        Connection dbConnection = Conexao.getConexao();
        Statement st;
        String select = "select " + campo + ", count(renavam) from veiculo group by " + campo;
        System.out.println(select);
        ArrayList<Veiculo> all = new ArrayList();
        Veiculo v = new Veiculo();
        try {
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(select);
            while (rs.next()) {
                if("numtipov".equals(campo)){
                    v.setNumChassi(TipoVeiculo.getById(rs.getInt(campo)).getDescricao());
                }
                else
                {
                    v.setNumChassi(TipoCombustivel.getById(rs.getInt(campo)).getDescricao());
                }
                v.setQuantLug(rs.getInt("count(renavam)"));
                all.add(v);
                v = new Veiculo();
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());;
        }
        
        return all;
    }
    
    
    public boolean insert()
    {
        
        Connection dbConnection = Conexao.getConexao();
        PreparedStatement ps = null;

        String insert = "insert into veiculo (renavam, placa, numchassi, anofab, quantlug, quilometragem, numtipoc, numtipov, datacad) values (?,?,?,?,?,?,?,?,?)";
        try {
            ps = dbConnection.prepareStatement(insert);
            ps.setLong(1, this.renavam);
            ps.setString(2, this.placa);
            ps.setString(3, this.numChassi);
            ps.setInt(4, this.anoFab);
            ps.setInt(5, this.quantLug);
            ps.setInt(6, this.quilometragem);
            ps.setInt(7, this.tipoComb.getId());
            ps.setInt(8, this.tipoVeiculo.getId());
            ps.setDate(9, (java.sql.Date) Date.from(Instant.now()));
            ps.execute();
            
        } catch (SQLException e) {
            e.printStackTrace();
            
            return false;
        }
        
        return true;
    }
    
    public boolean update()
    {
        
        Connection dbConnection = Conexao.getConexao();
        PreparedStatement ps = null;

        String up = "update veiculo set(placa = ?, numchassi = ?, anofab = ?, quantlug = ?, quilometragem = ?, numtipoc = ?, numtipov = ?) where (renavam = ?)";
        try {
            ps = dbConnection.prepareStatement(up);
            ps.setString(1, this.placa);
            ps.setString(2, this.numChassi);
            ps.setInt(3, this.anoFab);
            ps.setInt(4, this.quantLug);
            ps.setInt(5, this.quilometragem);
            ps.setInt(6, this.tipoComb.getId());
            ps.setInt(7, this.tipoVeiculo.getId());
            ps.setLong(8, this.renavam);
            ps.execute();
            
        } catch (SQLException e) {
            e.printStackTrace();
            
            return false;
        }
        
        return true;
    }
    
    public void getThis()
    {
        
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getNumChassi() {
        return numChassi;
    }

    public void setNumChassi(String numChassi) {
        this.numChassi = numChassi;
    }

    public Long getRenavam() {
        return renavam;
    }

    public void setRenavam(Long renavam) {
        this.renavam = renavam;
    }

    public int getAnoFab() {
        return anoFab;
    }

    public void setAnoFab(int anoFab) {
        this.anoFab = anoFab;
    }

    public int getQuilometragem() {
        return quilometragem;
    }

    public void setQuilometragem(int quilometragem) {
        this.quilometragem = quilometragem;
    }

    public int getQuantLug() {
        return quantLug;
    }

    public void setQuantLug(int quantLug) {
        this.quantLug = quantLug;
    }

    public TipoVeiculo getTipoVeiculo() {
        return tipoVeiculo;
    }

    public void setTipoVeiculo(TipoVeiculo tipoVeiculo) {
        this.tipoVeiculo = tipoVeiculo;
    }

    public TipoCombustivel getTipoComb() {
        return tipoComb;
    }

    public void setTipoComb(TipoCombustivel tipoComb) {
        this.tipoComb = tipoComb;
    }

    public boolean delete() {
        
        Connection dbConnection = Conexao.getConexao();
        Statement st;
        String select = "delete from veiculo where renavam =" + this.renavam;

        try {
            st = dbConnection.createStatement();
            st.executeQuery(select);
        } catch (Exception e) {
            
            return false;
        }
        
        return true;
    }
   
    
}
