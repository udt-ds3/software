package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

/**
 *
 * @author Aluno
 */
public class Pessoa {
    private Date dataCadastro, dataNascimento;
    private String nome, telefone;
    private int idp;
    
    public boolean insert(){
        String generatedColumns[] = {"idp"};
        
        Connection dbConnection = Conexao.getConexao();
        PreparedStatement ps = null;

        String insert = "insert into Pessoa (idp, dataCadastro, dataNascimento, nome, telefone) values (idcliente.nextval, ?, ?, ?, ? )";
        try {
            ps = dbConnection.prepareStatement(insert, generatedColumns);
            ps.setString(1, this.nome);
            ps.setString(2, this.telefone);
            ps.setDate(3, (java.sql.Date) this.dataCadastro);
            ps.setDate(4, (java.sql.Date) this.dataNascimento);
            
            
            
           
            
           
            ps.execute();
            ResultSet rs = ps.getGeneratedKeys();

            if (rs.next()) {
                setId(rs.getInt(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            
            return false;
        }
        
        return true;
    }
    
    public boolean update(){
        Connection dbConnection = Conexao.getConexao();
        PreparedStatement ps = null;

        String update = "update Pessoa set  dataCadastro = ?, nome = ?, telefone = ?, dataNascimento = ? where (idp = ?)";
        try {
            ps = dbConnection.prepareStatement(update);
            ps.setDate(1, (java.sql.Date) this.dataCadastro);
            ps.setString(2, this.nome);
            ps.setString(3, this.telefone);
            ps.setDate(4, (java.sql.Date) this.dataNascimento);
            ps.setInt(5, this.idp);
            
            ps.executeUpdate();
            
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            
            return false;
        }
        
        return true;
    }
    
    public boolean delete(){
        Connection dbConnection = Conexao.getConexao();
        Statement st;
        String select = "delete from pessoa where idp =" + this.idp;

        try{
            st = dbConnection.createStatement();
            st.executeQuery(select);
            
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        
        return true;
    }
    
    public void getThis(){
        Connection dbConnection = Conexao.getConexao();
        
        Statement st;
        String select = "select * from pessoa where idp="+ idp;
        
        try {
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(select);

                this.dataNascimento = rs.getDate("dataNascimento");
                this.dataCadastro = rs.getDate("dataCadastro");
                this.nome = rs.getString("nome");
                this.telefone = rs.getString("telefone");
                
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public Date getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }
    
    public String getDataNFormatada() {
        return dataNascimento.toLocaleString().split(" ")[0];
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public int getId() {
        return idp;
    }

    public void setId(int id) {
        this.idp= id;
    }
    
    public boolean containsText(String text) {
        if ((" " + this.nome + ", " + this.telefone + ". " + this.idp + ", " + this.dataNascimento + " - " + this.dataCadastro).toLowerCase().contains(" " + text.toLowerCase())) {
            return true;
        }
        return false;
    }
}
