package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ClienteMaior extends Cliente{
    private String cpf;
    
    public static ArrayList<Pessoa> getAll(){
        ArrayList<Pessoa> all = new ArrayList();
        
        Connection dbConnection = Conexao.getConexao();
        Statement st;
        String select = "select * from cliente inner join pessoa on pessoa.idp = cliente.id";
        ClienteMaior c = new ClienteMaior();
        try {
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(select);

            while (rs.next()) {
                c.setDataNascimento(rs.getDate("dataNascimento"));
                c.setDataCadastro(rs.getDate("dataCadastro"));
                c.setEndereco(Endereco.getById(rs.getInt("ide")));
                c.setId(rs.getInt("id"));
                c.setNome(rs.getString("nome"));
                c.setTelefone(rs.getString("telefone"));
                c.setTipoPagamento(TipoPagamento.getById(rs.getInt("tipopag")));
                //c.setTrajetos(Trajeto.getAllByClientId(rs.getInt("id")));
                c.setCpf(rs.getString("cpf"));
                all.add(c);
                c = new ClienteMaior();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return all;
    }
     
    public static ClienteMaior getById(int id){
        Connection dbConnection = Conexao.getConexao();
        Statement st;
        String select = "select * from cliente inner join pessoa on pessoa.idp = cliente.id where cliente.id=" + id;
        ClienteMaior c = new ClienteMaior();
        try {
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(select);
             if(rs.next()) {
                c.setDataNascimento(rs.getDate("dataNascimento"));
                c.setDataCadastro(rs.getDate("dataCadastro"));
                c.setEndereco(Endereco.getById(rs.getInt("ide")));
                c.setId(rs.getInt("id"));
                c.setNome(rs.getString("nome"));
                c.setTelefone(rs.getString("telefone"));
                c.setTipoPagamento(TipoPagamento.getById(rs.getInt("tipopag")));
                c.setCpf(rs.getString("cpf"));
             } 
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return c;
    }
    
    @Override
    public boolean insert(){  
        boolean intest = super.insert();
        boolean uptest = this.update();
        return (intest && uptest);
    }
    
    @Override
    public boolean update(){  
       super.update();
        
       Connection dbConnection = Conexao.getConexao();
       PreparedStatement ps = null;

       String update = "update Cliente set cpf = ? where (id = ?)";
       try {
            ps = dbConnection.prepareStatement(update);
            ps.setString(1, this.cpf);
            ps.setInt(2, this.getId());
            ps.executeUpdate();
            
       } catch (SQLException e) {
            System.out.println(e.getMessage());
            return false;
       }     
       return true;
    }    
    
    @Override
    public void getThis(){
        super.getThis();
        
        Connection dbConnection = Conexao.getConexao();
        Statement st;
        String select = "select * from cliente where id=" + this.getId();
        try {
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(select);
            this.cpf = rs.getString("cpf");
        } catch (SQLException e) {
            e.printStackTrace();
        }  
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
    
}
