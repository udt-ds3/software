package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class Cliente extends Pessoa{
   
    private Endereco endereco = new Endereco();
    private TipoPagamento tipoPagamento = new TipoPagamento();
    
    
    public static ArrayList<Pessoa> getAll(){
        ArrayList<Pessoa> all = new ArrayList();

        Connection dbConnection = Conexao.getConexao();
        Statement st;
        String select = "select * from cliente inner join pessoa on pessoa.idp = cliente.id";
        Cliente c = new Cliente();
        try {
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(select);

            while (rs.next()) {
                c.setDataNascimento(rs.getDate("dataNascimento"));
                c.setId(rs.getInt("id"));
                if(c.isMaior()){
                    c = ClienteMaior.getById(c.getId());
                }
                else{
                    c = ClienteMenor.getById(c.getId());
                }
                all.add(c);
                c = new Cliente();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return all;
    }
    
    public static Cliente getById(int id){
        Connection dbConnection = Conexao.getConexao();
        Statement st;
        String select = "select * from cliente inner join pessoa on pessoa.idp = cliente.id where id=" + id;
        Cliente c = new Cliente();
        try {
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(select);

                c.setDataNascimento(rs.getDate("dataNascimento"));
                c.setDataCadastro(rs.getDate("dataCadastro"));
                c.setEndereco(Endereco.getById(rs.getInt("ide")));
                c.setId(rs.getInt("id"));
                c.setNome(rs.getString("nome"));
                c.setTelefone(rs.getString("telefone"));
                c.setTipoPagamento(TipoPagamento.getById(rs.getInt("tipopag")));
                //c.setTrajetos(Trajeto.getAllByClientId(rs.getInt("id")));
                
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return c;
    }
    
    @Override
    public boolean insert(){
        super.insert();
        
        Connection dbConnection = Conexao.getConexao();
        PreparedStatement ps = null;

        String insert = "insert into cliente (id, ide, tipopag) values (?, ?, ? )";
        try {
            ps = dbConnection.prepareStatement(insert);
            ps.setInt(1, this.getId());
            ps.setInt(2, this.endereco.getId());
            ps.setInt(3, this.tipoPagamento.getId());
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
            
            return false;
        }
        return true;
    }
    
    @Override
    public boolean update(){
       super.update();
        
       Connection dbConnection = Conexao.getConexao();
       PreparedStatement ps = null;

       String update = "update Cliente set  endereco = ?, tipoPagamento = ? where (id = ?)";
       try {
            ps = dbConnection.prepareStatement(update);
            ps.setInt(1, this.endereco.getId());
            ps.setInt(2, this.tipoPagamento.getId());
            ps.setInt(3, this.getId());
            ps.executeUpdate();
            
       } catch (SQLException e) {
            System.out.println(e.getMessage());
            return false;
       }
        
       return true;
    }
    
    @Override
    public boolean delete(){
        Connection dbConnection = Conexao.getConexao();
        Statement st;
        String select = "delete from cliente where id =" + this.getId();

        try{
            st = dbConnection.createStatement();
            st.executeQuery(select);
            
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        
        return true;
    }
    
    @Override
    public void getThis(){
        super.getThis();
        
        Connection dbConnection = Conexao.getConexao();
        Statement st;
        String select = "select * from cliente where id=" + this.getId();
        try {
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(select);
            this.endereco = Endereco.getById(rs.getInt("ide"));
            this.tipoPagamento = TipoPagamento.getById(rs.getInt("tipopag"));
        } catch (SQLException e) {
            e.printStackTrace();
        }  
        
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public TipoPagamento getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamento tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }
    
    public String clienteCompleto() {
        return this.getNome() + ", " + this.getTelefone() + " (" + this.getDataNascimento() + "). " + tipoPagamento;
    }
    
    public boolean isMaior()
    {
        Date s = getDataNascimento();
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        df.setLenient(false); // aqui o pulo do gato

        Calendar a = Calendar.getInstance();
        a.setTime(new Date());//data maior

        Calendar b = Calendar.getInstance();
        b.setTime(s);// data menor

        a.add(Calendar.YEAR, -b.get(Calendar.YEAR));

        if (a.get(Calendar.YEAR) > 18) {
            return true;
        }
        return false;
    }
    
    @Override
    public String toString() {
        return this.getNome() + ", " + this.getTelefone() + " (" + this.getDataNascimento() + "). " + tipoPagamento;
    }
        
}
