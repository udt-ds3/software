package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ClienteMenor extends Cliente{
    
    private ClienteMaior responsavel = new ClienteMaior();
    
    
    public static ArrayList<Pessoa> getAll(){
        ArrayList<Pessoa> all = new ArrayList();
        
        Connection dbConnection = Conexao.getConexao();
        Statement st;
        String select = "select * from cliente inner join pessoa on pessoa.idp = cliente.id";
        ClienteMenor c = new ClienteMenor();
        try {
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(select);

            while (rs.next()) {
                c.setDataNascimento(rs.getDate("dataNascimento"));
                c.setDataCadastro(rs.getDate("dataCadastro"));
                c.setEndereco(Endereco.getById(rs.getInt("ide")));
                c.setId(rs.getInt("id"));
                c.setNome(rs.getString("nome"));
                c.setTelefone(rs.getString("telefone"));
                c.setTipoPagamento(TipoPagamento.getById(rs.getInt("tipopag")));
                //c.setTrajetos(Trajeto.getAllByClientId(rs.getInt("id")));
                c.setResponsavel(ClienteMaior.getById(rs.getInt("responsavel")));
                all.add(c);
                c = new ClienteMenor();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return all;
    }
    
    public static ClienteMenor getById(int id){
        Connection dbConnection = Conexao.getConexao();
        Statement st;
        String select = "select * from cliente inner join pessoa on pessoa.idp = cliente.id where id=" + id;
        ClienteMenor c = new ClienteMenor();
        try {
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(select);
                if(rs.next()){
                c.setDataNascimento(rs.getDate("dataNascimento"));
                c.setDataCadastro(rs.getDate("dataCadastro"));
                c.setEndereco(Endereco.getById(rs.getInt("ide")));
                c.setId(rs.getInt("id"));
                c.setNome(rs.getString("nome"));
                c.setTelefone(rs.getString("telefone"));
                c.setTipoPagamento(TipoPagamento.getById(rs.getInt("tipopag")));
                c.setResponsavel(ClienteMaior.getById(rs.getInt("responsavel")));
                }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return c;
    }
    
    @Override
    public boolean insert(){  
       boolean intest = super.insert();
       boolean uptest = this.update();
       return uptest && intest;
    }
    
    @Override
    public boolean update(){
       super.update();
       
       Connection dbConnection = Conexao.getConexao();
       PreparedStatement ps = null;

       String update = "update Cliente set responsavel = ? where (id = ?)";
       try {
            ps = dbConnection.prepareStatement(update);
            ps.setInt(1, this.responsavel.getId());
            ps.setInt(2, this.getId());
            ps.executeUpdate();
            
       } catch (SQLException e) {
            System.out.println(e.getMessage());
            return false;
       }     
       return true;
    }    
    
    @Override
    public void getThis(){
        super.getThis();
        
        Connection dbConnection = Conexao.getConexao();
        Statement st;
        String select = "select * from cliente where id=" + this.getId();
        try {
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(select);
            this.responsavel = ClienteMaior.getById(rs.getInt("responsavel"));
        } catch (SQLException e) {
            e.printStackTrace();
        }  
    }

    public ClienteMaior getResponsavel() {
        return responsavel;
    }

    public void setResponsavel(ClienteMaior responsavel) {
        this.responsavel = responsavel;
    }
    
}
