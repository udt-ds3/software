package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Aluno
 */
public class Endereco {

  

    private String pais, estado, cidade, bairro, rua, cep, complemento = "";
    private int id, numero, tempo;

    /**
     * Pega todos endereços do banco.
     */
    public static ArrayList<Endereco> getAll() {

        ArrayList<Endereco> all = new ArrayList();

        
        Connection dbConnection = Conexao.getConexao();
        Statement st;
        String select = "Select * From endereco";
        Endereco ende = new Endereco();
        try {
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(select);

            while (rs.next()) {
                ende.setPais(rs.getString("pais"));
                ende.setEstado(rs.getString("estado"));
                ende.setCidade(rs.getString("cidade"));
                ende.setBairro(rs.getString("bairro"));
                ende.setRua(rs.getString("rua"));
                ende.setCep(rs.getString("cep"));
                ende.setComplemento(rs.getString("complemento"));
                ende.setId(rs.getInt("id"));
                ende.setNumero(rs.getInt("numero"));
                ende.setTempo(rs.getInt("tempoDeslocamento"));
                all.add(ende);
                ende = new Endereco();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return all;
    }

    /**
     * Pega um endereços do banco pelo ID.
     */
    public static Endereco getById(int id) {
        
        Connection dbConnection = Conexao.getConexao();
        Statement st;
        System.out.println(id);
        String select = "Select * From endereco where id =" + id;
        Endereco ende = new Endereco();
        try {
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(select);
            if(rs.next()){
                ende.setPais(rs.getString("pais"));
                ende.setEstado(rs.getString("estado"));
                ende.setCidade(rs.getString("cidade"));
                ende.setBairro(rs.getString("bairro"));
                ende.setRua(rs.getString("rua"));
                ende.setCep(rs.getString("cep"));
                ende.setComplemento(rs.getString("complemento"));
                ende.setId(id);
                ende.setNumero(rs.getInt("numero"));
                ende.setTempo(rs.getInt("tempodeslocamento"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        
        return ende;
    }

    /**
     * Insere um endereço no banco.
     */
    public boolean insert() {
        String generatedColumns[] = {"id"};
        
        Connection dbConnection = Conexao.getConexao();
        PreparedStatement ps = null;

        String insert = "insert into endereco (id, pais, estado, cidade, bairro, rua, cep, numero, complemento, tempodeslocamento) values (idendereco.nextval, ?,?,?,?,?,?,?,?,?)";
        try {
            ps = dbConnection.prepareStatement(insert, generatedColumns);
            ps.setString(1, this.pais);
            ps.setString(2, this.estado);
            ps.setString(3, this.cidade);
            ps.setString(4, this.bairro);
            ps.setString(5, this.rua);
            ps.setString(6, this.cep);
            ps.setInt(7, this.numero);
            ps.setString(8, this.complemento);
            ps.setInt(9, this.tempo);
            ps.execute();
            ResultSet rs = ps.getGeneratedKeys();

            if (rs.next()) {
                id = rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            
            return false;
        }
        
        return true;
    }

    /**
     * Atualiza um endereço no banco.
     */
    public boolean update() {
        
        Connection dbConnection = Conexao.getConexao();
        PreparedStatement ps = null;

        String update = "update endereco set pais = ?, estado = ?, cidade = ?, bairro = ?, rua = ?, cep = ?, complemento = ?, numero = ?, tempoDeslocamento = ? where (id = ?)";
        try {
            ps = dbConnection.prepareStatement(update);
            ps.setString(1, this.pais);
            ps.setString(2, this.estado);
            ps.setString(3, this.cidade);
            ps.setString(4, this.bairro);
            ps.setString(5, this.rua);
            ps.setString(6, this.cep);
            ps.setString(7, this.complemento);
            ps.setInt(8, this.numero);
            ps.setInt(9, this.tempo);
            ps.setInt(10, this.id);
            ps.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            
            return false;
        }
        
        return true;
    }

    /**
     * Vê se o endereço possui um texto.
     */
    public boolean containsText(String text) {
        if ((" " + this.rua + ", " + this.numero + ". " + this.bairro + ", " + this.cidade + " - " + this.estado + ". - " + this.cep).toLowerCase().contains(" " + text.toLowerCase())) {
            return true;
        }
        return false;
    }

    /**
     * Captura um grupo de endereços por um ou mais campos.
     */
    public static ArrayList<Endereco> getGroupBy(String campo) {
        
        Connection dbConnection = Conexao.getConexao();
        Statement st;
        String select = "select " + campo + ", count(tempodeslocamento), avg(tempodeslocamento) from endereco group by " + campo;
        System.out.println(select);
        ArrayList<Endereco> all = new ArrayList();
        Endereco ende = new Endereco();
        try {
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(select);
            while (rs.next()) {
                for (String s : campo.split(", ")) {
                    ende.setComplemento(ende.getComplemento() + rs.getString(s) + "; ");
                }
                ende.setNumero(rs.getInt("count(tempodeslocamento)"));
                ende.setTempo(rs.getInt("avg(tempodeslocamento)"));
                all.add(ende);
                ende = new Endereco();
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());;
        }
        
        return all;
    }

    /**
     * Pega os dados desse endereço no banco.
     */
    public void getThis() {
        
        Connection dbConnection = Conexao.getConexao();
        Statement st;
        String select = "Select * From endereco where id =" + this.id;

        try {
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(select);
            this.pais = rs.getString("pais");
            this.estado = rs.getString("estado");
            this.cidade = rs.getString("cidade");
            this.bairro = rs.getString("bairro");
            this.rua = rs.getString("rua");
            this.cep = rs.getString("cep");
            this.complemento = rs.getString("complemento");
            this.numero = rs.getInt("numero");
            this.tempo = rs.getInt("tempo");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getRua() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public int getTempo() {
        return tempo;
    }

    public void setTempo(int tempo) {
        this.tempo = tempo;
    }

    @Override
    public String toString() {
        return cep + ": " + rua + ", " + bairro + ", " + cidade + " - " + estado;
    }

    public String enderecoCompleto() {
        return rua + ", " + numero + " (" + complemento + "). " + bairro + ", " + cidade + " - " + estado + ". " + cep;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = (complemento == null) ? "" : complemento;
    }

    public boolean delete() {
        
        Connection dbConnection = Conexao.getConexao();
        Statement st;
        String select = "delete from endereco where id =" + this.id;

        try {
            st = dbConnection.createStatement();
            st.executeQuery(select);
        } catch (Exception e) {
            
            return false;
        }
        
        return true;
    }

  

}
